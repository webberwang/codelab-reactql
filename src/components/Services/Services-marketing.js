/* eslint max-len: ["error", { "ignoreStrings": true }] */

import React, { PureComponent } from 'react';
import { Container, Grid, Image } from 'semantic-ui-react';
import WebdesignFlaticon from '../svg/WebdesignFlaticon';
import Marketing from '../svg/Marketing';
import Blogging from '../svg/Blogging';
import Analysis from '../svg/Analysis';
import Fax from '../svg/Fax';
import BrandingFlaticon from '../svg/BrandingFlaticon';
import Report from '../svg/Report';

class Header extends PureComponent {
  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
  }

  state = {
    marketingIcon: {
      path: {
        fill: 'green',
      },
    },
  };

  render() {
    return (
      <section
        className="Section Section--minFull Service Bg--blue Text--white">
        <div className="Section-content">
          {/*<div className="Navigation-arrow Navigation-arrow--left">*/}
          {/*<div className="Navigation-arrowContent">*/}
          {/*<Image src="/img/graphics/Arrow-left.svg" />*/}
          {/*<span> Marketing </span>*/}
          {/*</div>*/}
          {/*</div>*/}
          {/*<div className="Navigation-arrow Navigation-arrow--right">*/}
          {/*<div className="Navigation-arrowContent">*/}
          {/*<Image src="/img/graphics/Arrow-right.svg" />*/}
          {/*<span> Development </span>*/}
          {/*</div>*/}
          {/*</div>*/}
          <div className="Navigation-arrow Navigation-arrow--down">
            <div className="Navigation-arrowContent">
              <Image src="/img/graphics/Arrow-down.svg" />
              <span> Development </span>
            </div>
          </div>
          <div className="container">
            <div className="Section-heading">
              <div>
                <WebdesignFlaticon />
              </div>
              <h1> Digital Marketing </h1>
              <p>
                If you already have a website fine-tuned for conversion, the
                next step is to starting bring in traffic to your site. There
                are organic, paid, inbound, and outbound methods for getting
                people to your site.
              </p>
            </div>
            <div className="row">
              <div className="col-sm-12 col-md-6 col-lg-4">
                <article className="Service-item">
                  <h4 className="Service-itemTitle"> Pay-Per-Click Campaigns
                    (PPC)
                  </h4>
                  <div className="Service-itemContent">
                    <div>
                      <Marketing style={this.state.marketingIcon} />
                    </div>
                    <p>
                      Google AdWords, Facebook ads, & LinkedIn ads are all
                      platforms capable of high ROI's when used correctly.
                    </p>
                  </div>
                </article>
              </div>
              <div className="col-sm-12 col-md-6 col-lg-4">
                <article className="Service-item">
                  <h4 className="Service-itemTitle"> Content Creation </h4>
                  <div className="Service-itemContent">
                    <div>
                      <Blogging />
                    </div>
                    <p>
                      Content is the bedrock of a good SEO campaign. Valuable
                      content also gets shared across the web.
                    </p>
                  </div>
                </article>
              </div>
              <div className="col-sm-12 col-md-6 col-lg-4">
                <article className="Service-item">
                  <h4 className="Service-itemTitle"> SEO Optimization </h4>
                  <div className="Service-itemContent">
                    <div>
                      <Analysis />
                    </div>
                    <p>
                      There are hundreds of factors that affect SEO. The best we
                      can do is optimize the factors that we know are certain.
                    </p>
                  </div>
                </article>
              </div>
              <div className="col-sm-12 col-md-6 col-lg-4">
                <article className="Service-item">
                  <h4 className="Service-itemTitle"> Email & Cold Call
                    Outreach
                  </h4>
                  <div className="Service-itemContent">
                    <div>
                      <Fax />
                    </div>
                    <p>
                      We'll reach more potential customers for your business by
                      emailing & cold calling them.
                    </p>
                  </div>
                </article>
              </div>
              <div className="col-sm-12 col-md-6 col-lg-4">
                <article className="Service-item">
                  <h4 className="Service-itemTitle"> Social Media
                    Management
                  </h4>
                  <div className="Service-itemContent">
                    <div>
                      <BrandingFlaticon />
                    </div>
                    <p>
                      The social media platforms are great for customer
                      engagement and sales conversion.
                    </p>
                  </div>
                </article>
              </div>
              <div className="col-sm-12 col-md-6 col-lg-4">
                <article className="Service-item">
                  <h4 className="Service-itemTitle"> Keyword & Competitor
                    Analysis
                  </h4>
                  <div className="Service-itemContent">
                    <div>
                      <Report />
                    </div>
                    <p>
                      The prelimniary step is to stategize a plan for success.
                    </p>
                  </div>
                </article>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Header;
