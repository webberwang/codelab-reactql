/* eslint max-len: ["error", { "ignoreStrings": true }] */

import React, { PureComponent } from 'react';
import { Container, Grid, Image } from 'semantic-ui-react';
import ArrowDown from '../svg/ArrowDown';
import WebdesignFlaticon from '../svg/WebdesignFlaticon';
import WordpressBigLogo from '../svg/WordpressBigLogo';
import Browser from '../svg/Browser';
import EarthGlobe from '../svg/EarthGlobe';
import OnlineShop from '../svg/OnlineShop';
import Responsive from '../svg/Responsive';
import Api from '../svg/Api';
import Wordpress from '../svg/Wordpress';

class Header extends PureComponent {
  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
  }

  state = {};

  render() {
    return (
      <section
        className="Section Section--minFull Service">
        <div className="Section-content">
          {/*<div className="Navigation-arrow Navigation-arrow--left">*/}
          {/*<div className="Navigation-arrowContent">*/}
          {/*<Image src="/img/graphics/Arrow-left.svg" />*/}
          {/*<span> Marketing </span>*/}
          {/*</div>*/}
          {/*</div>*/}
          {/*<div className="Navigation-arrow Navigation-arrow--right">*/}
          {/*<div className="Navigation-arrowContent">*/}
          {/*<Image src="/img/graphics/Arrow-right.svg" />*/}
          {/*<span> Development </span>*/}
          {/*</div>*/}
          {/*</div>*/}
          <div className="Navigation-arrow Navigation-arrow--down">
            <div className="Navigation-arrowContent">
              <div>
                <ArrowDown />
              </div>
              <span> Development </span>
            </div>
          </div>
          <div className="container">
            <div className="Section-heading">
              <div>
                <WebdesignFlaticon />
              </div>
              <h1> Web Application Development </h1>
              <p>
                Once you have the designs & specifications ready, we can start
                coding and develop the web software. Websites can range from a
                simple landing page, to a full-blown custom app. Every project
                is unique so our proposal differs on a case to case basis.
              </p>
            </div>
            <div className="row">
              <div className="col-sm-12 col-md-6 col-lg-4">
                <article className="Service-item">
                  <h4 className="Service-itemTitle"> CMS Management </h4>
                  <div className="Service-itemContent">
                    <div>
                      <Wordpress />
                    </div>
                    <p> Need to manage blogs for your business? We'll install a
                      Wordpress site for your content management needs
                    </p>
                  </div>
                </article>
              </div>
              <div className="col-sm-12 col-md-6 col-lg-4">
                <article className="Service-item">
                  <h4 className="Service-itemTitle"> Single Landing Page </h4>
                  <div className="Service-itemContent">
                    <div>
                      <Browser />
                    </div>
                    <p>
                      A single page landing form with a engaging banner,
                      detailed case study, & a contact form will help greatly
                      with testing out products & increasing conversion.
                    </p>
                  </div>
                </article>
              </div>
              <div className="col-sm-12 col-md-6 col-lg-4">
                <article className="Service-item">
                  <h4 className="Service-itemTitle"> Standard Website </h4>
                  <div className="Service-itemContent">
                    <div>
                      <EarthGlobe />
                    </div>
                    <p> These sites include standard pages like Home, About,
                      Services, & Contact. They're good for showcasing your
                      portfolio or business offers.
                    </p>
                  </div>
                </article>
              </div>
              <div className="col-sm-12 col-md-6 col-lg-4">
                <article className="Service-item">
                  <h4 className="Service-itemTitle"> E-Commerce Solutions </h4>
                  <div className="Service-itemContent">
                    <div>
                      <OnlineShop />
                    </div>
                    <p>
                      Need to sell products? We'll set up a Shopify or
                      WooCommerce store for you to list products.
                    </p>
                  </div>
                </article>
              </div>
              <div className="col-sm-12 col-md-6 col-lg-4">
                <article className="Service-item">
                  <h4 className="Service-itemTitle"> Custom Web
                    Application
                  </h4>
                  <div className="Service-itemContent">
                    <div>
                      <Responsive />
                    </div>
                    <p>
                      Sites that need custom business logic, such as a food
                      delivery website or a custom dashboard are considered web
                      applications.
                    </p>
                  </div>
                </article>
              </div>
              <div className="col-sm-12 col-md-6 col-lg-4">
                <article className="Service-item">
                  <h4 className="Service-itemTitle"> Backend API
                    Development
                  </h4>
                  <div className="Service-itemContent">
                    <div>
                      <Api />
                    </div>
                    <p>
                      We'll integrate API's to your business to help out with
                      automation.
                    </p>
                  </div>
                </article>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Header;
