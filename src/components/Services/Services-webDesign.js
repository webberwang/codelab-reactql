/* eslint max-len: ["error", { "ignoreStrings": true }] */

import React, { PureComponent } from 'react';
import { Container, Grid, Image } from 'semantic-ui-react';
import WebdesignFlaticon from '../svg/WebdesignFlaticon';
import BounceRateFlaticon from '../svg/BounceRateFlaticon';
import ConversionFlaticon from '../svg/ConversionFlaticon';
import BrandingFlaticon from '../svg/BrandingFlaticon';

class Header extends PureComponent {
  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
  }

  state = {};

  render() {
    return (
      <section
        className="Section Section--minFull Service Bg--blue Text--white">
        <div className="Section-content">
          {/*<div className="Navigation-arrow Navigation-arrow--left">*/}
          {/*<div className="Navigation-arrowContent">*/}
          {/*<Image src="/img/graphics/Arrow-left.svg" />*/}
          {/*<span> Marketing </span>*/}
          {/*</div>*/}
          {/*</div>*/}
          {/*<div className="Navigation-arrow Navigation-arrow--right">*/}
          {/*<div className="Navigation-arrowContent">*/}
          {/*<Image src="/img/graphics/Arrow-right.svg" />*/}
          {/*<span> Development </span>*/}
          {/*</div>*/}
          {/*</div>*/}
          <div className="Navigation-arrow Navigation-arrow--down">
            <div className="Navigation-arrowContent">
              <Image src="/img/graphics/Arrow-down.svg" />
              <span> Development </span>
            </div>
          </div>
          <div className="container">
            <div className="Section-heading">
              <div>
                <WebdesignFlaticon />
              </div>
              <h1> Web Design + UI/UX Prototype </h1>
              <p> If you have a design or style in mind, it’ll make it easier
                for us to match your taste. Otherwise, we’ll
                create a custom template to tailor to your business. The whole
                process is an iterative approach, with each
                revision bringing closer to the final product.
              </p>
            </div>
            <div className="row">
              <div className="col-sm-12 col-md-6 col-lg-4">
                <article className="Service-item">
                  <h4 className="Service-itemTitle"> Reduce Bounce Rates </h4>
                  <div className="Service-itemContent">
                    <div>
                      <BounceRateFlaticon />
                    </div>
                    <p> 50% of users leave a website within 8 seconds. We keep
                      your
                      users engaged so they stay on the
                      site.
                    </p>
                  </div>
                </article>
              </div>
              <div className="col-sm-12 col-md-6 col-lg-4">
                <article className="Service-item">
                  <h4 className="Service-itemTitle"> Maximize Conversion </h4>
                  <div className="Service-itemContent">
                    <div>
                      <ConversionFlaticon />
                    </div>
                    <p> 2.35% is the average landing page conversion rate across
                      industries. We exceed that
                      consistently.
                    </p>
                  </div>
                </article>
              </div>
              <div className="col-sm-12 col-md-6 col-lg-4">
                <article className="Service-item">
                  <h4 className="Service-itemTitle"> Enhance Branding </h4>
                  <div className="Service-itemContent">
                    <div>
                      <BrandingFlaticon />
                    </div>
                    <p> Consumers make decisions based on their emotional
                      reaction
                      to brands. Strengthen your brand with
                      clean & modern design.
                    </p>
                  </div>
                </article>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Header;
