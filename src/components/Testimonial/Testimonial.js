import React, { PureComponent } from 'react';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import PropTypes from 'prop-types';
import { Paper } from 'material-ui';
import Draggable, { DraggableCore } from 'react-draggable'; // Both at the same time
import { slide as Menu } from 'react-burger-menu';
import { Grid, Image } from 'semantic-ui-react';
import Slider from 'react-slick';
import './Testimonial.global.scss';


class Testimonial extends React.Component {
  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
  }

  state = {
    images: [
      '/img/logos/5element.png',
      '/img/logos/alpha-group.png',
      '/img/logos/atlanta-wheel-repair.png',
      '/img/logos/foodhaus.png',
      '/img/logos/nodusk.png',
      '/img/logos/repenthaus.png',
      '/img/logos/synergy.png',
    ],
    settings: {
      dots: true,
      infinite: false,
      speed: 500,
      slidesToScroll: 2,
      touchThreshold: 28,
      responsive: [
        { breakpoint: 576, settings: { slidesToShow: 2 } },
        { breakpoint: 768, settings: { slidesToShow: 3 } },
        {
          breakpoint: 992,
          settings: { slidesToShow: 4 },
        },
        {
          breakpoint: 1120991,
          settings: { slidesToShow: 5, slidesToScroll: 3 },
        },
      ],
    },
  };


  render() {
    return (
      <div className="Testimonial">
        <h5 className="text-center"> Codelab Work With </h5>
        <Grid className="">
          <Grid.Column textAlign="center">
            <Slider {...this.state.settings}>
              {(this.state.images.map(src => (
                <div>
                  <div className="Testimonial-imgWrapper"><Image draggable="false" inline src={src} key={src} /></div>
                </div>
              )))}
            </Slider>
          </Grid.Column>
        </Grid>
      </div>
    );
  }
}

export default Testimonial;
