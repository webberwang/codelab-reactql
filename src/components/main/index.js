// Main React component, that we'll import in `src/app.js`
//
// Note a few points from this file:
//
// 1.  We're using the format `main/index.js` for this file, which means we
// can simply import 'src/components/main', which will auto-default to index.js.
// This is a useful pattern when you have styles/images to pull from, and you
// want to keep the component tree organised.
//
// 2.  We import SASS and a logo SVG file directly.  Classnames will be hashed
// automatically, and images will be compressed/optimised in production.  File
// names that are made available in the import variable will be identical on
// both the server and browser.
//
// 3.  We're introducing React Router in this component.  In RR v4, routes are
// not defined globally-- they're defined declaratively on components, so we
// can respond to route changes anywhere.
//
// 4.  We're using `react-helmet`, which allows us to set <head> data like
// a <title> or <meta> tags, which are filtered up to the main <Html> component
// before HTML rendering.

// ----------------------
// IMPORTS
import 'src/components/styles/styles.global.scss';

// require('../../vendor/fontawesome-all.js');

/* NPM */

// React
import React from 'react';
// Routing via React Router
// <Helmet> component for setting the page title/meta tags
import Helmet from 'react-helmet';
// NotFound 404 handler for unknown routes, and the app-wide `history` object
// we can use to make route changes from anywhere
import { history } from 'kit/lib/routing';
// Child React components. Note:  We can either export one main React component
// per file, or in the case of <Home>, <Page> and <WhenFound>, we can group
// multiple components per file where it makes sense to do so
import { Home, Page, WhenNotFound } from 'components/routes';
// Get the ReactQL logo.  This is a local .svg file, which will be made
// available as a string relative to [root]/dist/assets/img/

import GraphQLCMS from '../graphql';

/* ReactQL */

/* Vendor */
import {
  Parallax,
  ParallaxBanner,
  ParallaxProvider
} from 'react-scroll-parallax';

// import 'semantic-ui-css/semantic.min.css';

import Header from '../Header/Header';
import Banner from '../Header/Banner';
import Banner2 from '../Header/Banner2';
import Testimonial from '../Testimonial/Testimonial';
import { Container, Grid, Image } from 'semantic-ui-react';
import ServicesWebDesign from '../Services/Services-webDesign';
import ServicesWebDevelopment from '../Services/Services-webDevelopment';
import ServicesMarketing from '../Services/Services-marketing';


var Slider = require('react-slick');

// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941


/* App */

// Styles
// import css from './main.global.css';
// require('blint-react');

// ----------------------

// Example function to show that the `history` object can be changed from
// anywhere, simply by importing it-- use this in Redux actions, functions,
// React `onClick` events, etc.
function changeRoute() {
  history.push('/page/about');
}

const settings = {
  dots: true,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
};

export default () => (

  <ParallaxProvider>
    <main>
      <Helmet>
        <title>ReactQL application</title>
        <meta name="description" content="ReactQL starter kit app" />
        {/* <base href="http://localhost:8081/" /> */}
      </Helmet>
      {/* <div className={css.hello}>
        <img src={logo} alt="ReactQL" className={css.logo} />
      </div> */}
      {/* <GraphQLMessage /> */}
      {/* <GraphQLCMS /> */}
      <Header />
      {/*<nav />*/}
      <Banner />
      <ServicesWebDesign />
      <ServicesWebDevelopment />
      <ServicesMarketing />
      {/*<Testimonial />*/}
      <Container>
        <Grid>
          <Grid.Column>
            <h1> We <span
              className="Gradient Gradient-text Gradient-text--green"> transform </span>
              <span
                className="Gradient Gradient-text Gradient-text--red"> ideas </span> into
              <span
                className="Gradient Gradient-text Gradient-text--blue"> products </span>
            </h1>
          </Grid.Column>
        </Grid>
        <Image width="100" src="/img/graphics/icon_43.svg" />
        <h2> Project Planning </h2>
        <p> The initial phase is where we sit down with the customer & talk
          about the idea and vision. What goals are we
          trying to accomplish, and what are the business requirements. All of
          these important questions will be
          answered with the end user in mind. </p>
        <Image width="100" src="/img/graphics/icon_19.svg" />
        <h2> UI Design & Prototype </h2>
        <p> With the </p>
        <Image width="100" src="/img/graphics/icon_15.svg" />
        <h2> Development & Bug Test</h2>
        <Image width="100" src="/img/graphics/icon_9.svg" />
        <h2> Quality Assurance & UX </h2>
      </Container>
      <div style={{ minHeight: '40rem' }}></div>
      {/*<Banner2 />*/}

    </main>
  </ParallaxProvider>
);
