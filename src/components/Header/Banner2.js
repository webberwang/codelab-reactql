import React from 'react';
import { ParallaxBanner } from 'react-scroll-parallax';


const Banner = () => (
  <ParallaxBanner
    className="your-class"
    layers={[
      {
        image: 'img/backgrounds/tablets-camera-tabletop.jpg',
        amount: 0.1,
        slowerScrollRate: false,
      },
      {
        image: 'img/backgrounds/overlay.png',
        amount: 0.2,
        slowerScrollRate: false,
      },
    ]}
    style={{
      height: '500px',
    }}>
    <h1>Banner Children</h1>
  </ParallaxBanner>
);

export default Banner;

