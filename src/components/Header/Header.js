import React, { PureComponent } from 'react';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import PropTypes from 'prop-types';
import { Paper } from 'material-ui';
import Draggable, { DraggableCore } from 'react-draggable'; // Both at the same time
import { slide as Menu } from 'react-burger-menu';

class Header extends PureComponent {
  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.isMenuOpen = this.isMenuOpen.bind(this);
  }

  state = {
    isOpen: false,
  };

  isMenuOpen(state) {
    this.setState({ isOpen: state.isOpen });
  }

  render() {
    return (
      <header className="Header">
        <Menu
          right
          customBurgerIcon={
            <div>
              <div className={`Menu ${this.state.isOpen ? 'isOpen' : ''}`}>
                <span className="Menu-bar Menu-bar--top" />
                <span className="Menu-bar Menu-bar--mid" />
                <span className="Menu-bar Menu-bar--bot" />
              </div>
            </div>
          }
          customCrossIcon={<span> <i className="far fa-times" /> </span>}
          onStateChange={this.isMenuOpen}>
          <a id="home" className="menu-item" href="/">Home</a>
          <a id="about" className="menu-item" href="/about">About</a>
          <a id="contact" className="menu-item" href="/contact">Contact</a>
        </Menu>
        {/*<nav*/}
        {/*onScroll={this.handleScroll}*/}
        {/*style={this.state.navStyle}*/}
        {/*className={`Menu ${this.state.showMenu ? '' : 'isHidden'}`}>*/}

        {/*<div className="Menu-item" style={this.state.style1}>*/}
        {/*<FloatingActionButton backgroundColor={pink200}>*/}
        {/*<i className="far fa-envelope-open" />*/}
        {/*</FloatingActionButton>*/}
        {/*</div>*/}
        {/*<div className="Menu-item" style={this.state.style2}>*/}
        {/*<FloatingActionButton backgroundColor={purple200} style={this.state.style2}>*/}
        {/*<i className="far fa-briefcase" />*/}
        {/*</FloatingActionButton>*/}
        {/*</div>*/}
        {/*<div className="Menu-item" style={this.state.style3}>*/}
        {/*<FloatingActionButton backgroundColor={lightBlue200} style={this.state.style3}>*/}
        {/*<i className="far fa-phone" />*/}
        {/*</FloatingActionButton>*/}
        {/*</div>*/}
        {/*<div className="Menu-item" style={this.state.style4}>*/}
        {/*<FloatingActionButton backgroundColor={orange200} style={this.state.style4}>*/}
        {/*<i className="far fa-envelope-open" />*/}
        {/*</FloatingActionButton>*/}
        {/*</div>*/}
        {/*</nav>*/}
      </header>
    );
  }
}

export default Header;
