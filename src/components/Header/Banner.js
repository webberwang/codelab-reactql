import React from 'react';
import { Parallax, Background } from 'react-parallax';


const Banner = () => (
  <div className="Banner Section Section--full">
    <div className="Section-content Section-content--noPadding">
      <Parallax
        bgImage="img/backgrounds/tablets-camera-tabletop.jpg"
        bgWidth="auto"
        bgHeight="auto"
        strength={500}>
        <div className="Banner-overlay" />
        <div className="Banner-content">
          <h1 className="text-center"> Codelab Studios is a web development
            agency.
          </h1>
          <p> We bring design to life & help start ups grow. </p>
        </div>
      </Parallax>
    </div>
  </div>
);

export default Banner;

