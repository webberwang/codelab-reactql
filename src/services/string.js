export const sentence = string => (
  string.charAt(0).toUpperCase() + string.slice(1)
);

export const slug = string => (
  string.toString().toLowerCase()
    .replace(/\s+/g, '-')
    .replace(/[^\w-]+/g, '')
    .replace(/--+/g, '-')
    .replace(/^-+/, '')
    .replace(/-+$/, '')
);

export default {};

